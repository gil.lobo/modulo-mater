<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modulo extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'fch_registro',
        'user_id',
        'iturno',
        'iedad',
        'capaterno',
        'camaterno',
        'cnombre',
        'itrimestre',
        'igestas',
        'csdg',
        'ipartos',
        'icesareas',
        'iabortos',
        'iprincipa',
        'isecundario1',
        'isecundario2',
        'ihospi_codigo',
        'itermina_embarazo',
        'iprocedimiento',
        'ireintervencion1',
        'ireintervencion2',
        'ireintervencion3',
        'ireintervencion4',
        'ireintervencion5',
        'itraslado',
        'ifuera_estado',
        'iapeo',
        'imorbilidad',
        'imortalidad',
        'cobservaciones',
        'ctelefono',
        'ientidad',
        'iderechohabiencia',
        'ireferencia',
        'iindigena',
        'ihemoderivados',
        'ihemoderivados2',
        'cegreso',
        'cmejoria',
        'creferencia',
        'ipuerperio',
        'imuerte_materna'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActualizaRegistro extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'modulo_id',
    ];
    
}

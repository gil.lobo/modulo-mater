@extends('layouts.app')
@section('title')
| Módulo Mater
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading">Actualizar Registro</h3>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            @if($errors->any())
                                <div class="alert alert-dark alert-dismissible fade show" role="alert">
                                    <strong>¡Revisa los Campos!</strong>
                                    @foreach($errors->all() as $error)
                                        <span class="badge badge-danger">{{$error}}</span>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            {!! Form::model($Modulo, ['method'=>'PATCH', 'route'=>['modulo-mater.update', $Modulo->id]]) !!}
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="name">Fecha</label>
                                        {!! Form::date('fch_registro', null, array('class'=>'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="roles">Turno</label>
                                        {!! Form::select('iturno', [
                                                                        '1' => 'Matutino', 
                                                                        '2' => 'Vespertino',
                                                                        '3' => 'Nocturo',
                                                                        '4' => 'Jornada Especial Diurna',
                                                                        '5' => 'Jornada Especial Nocturna'
                                                                    ], null, ['placeholder' => 'Seleccione...', 'class'=>'form-control']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="name">Edad</label>
                                        {!! Form::text('iedad', null, array('class'=>'form-control')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="name">A. Paterno</label>
                                        {!! Form::text('capaterno', null, array('class'=>'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="name">A. Materno</label>
                                        {!! Form::text('camaterno', null, array('class'=>'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="name">Nombre</label>
                                        {!! Form::text('cnombre', null, array('class'=>'form-control')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                    <label for="roles">Entidad</label>
                                        {!! Form::select('ientidad', $Entidad,$Modulo->ientidad, ['class'=>'form-control']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">Teléfono de Contacto</label>
                                        {!! Form::text('ctelefono', null, array('class'=>'form-control')); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">Derechohabiencia</label>
                                        {!! Form::select('iderechohabiencia', [
                                                                        '1' => 'IMSS', 
                                                                        '2' => 'ISSSTE',
                                                                        '3' => 'SEDENA',
                                                                        '4' => 'INSABI',
                                                                        '5' => 'PEMEX',
                                                                        '6' => 'NINGUNA'
                                                                    ], null, ['placeholder' => 'Seleccione...', 'class'=>'form-control']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">Referencia</label>
                                        {!! Form::select('ireferencia', [
                                                                        '1' => 'Si', 
                                                                        '2' => 'No',
                                                                        
                                                                    ], null, ['placeholder' => 'Seleccione...', 'class'=>'form-control']); !!}
                                    </div>
                                </div>  
                                                              
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">Indigena</label>
                                        {!! Form::select('iindigena', [
                                                                        '1' => 'Si', 
                                                                        '2' => 'No',
                                                                        
                                                                    ], null, ['placeholder' => 'Seleccione...', 'class'=>'form-control']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">SDG</label>
                                        {!! Form::text('csdg', null, array('class'=>'form-control')); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">Trimestre</label>
                                        {!! Form::text('itrimestre', null, array('class'=>'form-control')); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">Gestas</label>
                                        {!! Form::text('igestas', null, array('class'=>'form-control')); !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">Puerperio</label>
                                        {!! Form::select('ipuerperio', [
                                                                        '1' => 'Si', 
                                                                        '2' => 'No',
                                                                        
                                                                    ], null, ['placeholder' => 'Seleccione...', 'class'=>'form-control']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">Partos</label>
                                        {!! Form::text('ipartos', null, array('class'=>'form-control')); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">Cesáreas</label>
                                        {!! Form::text('icesareas', null, array('class'=>'form-control')); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">Abortos</label>
                                        {!! Form::text('iabortos', null, array('class'=>'form-control')); !!}
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                    <label for="roles">Diagnóstico Principal</label>
                                        {!! Form::select('iprincipa', $Prima, $Modulo->principa, ['title' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                    <label for="roles">Secundario 1</label>
                                    {!! Form::select('isecundario1', $Sec, $Modulo->isecundario1, ['title' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                    <label for="roles">Secundario 2</label>
                                        {!! Form::select('isecundario2', $Sec, $Modulo->isecundario2, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                    <label for="roles">Lugar del Código</label>
                                        {!! Form::select('ihospi_codigo', $Hosp, $Modulo->ihospi_codigo, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                    <label for="roles">Vía de Terminación del Embarazo</label>
                                        {!! Form::select('itermina_embarazo', $Termi, $Modulo->itermina_embarazo,  ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                    <label for="roles">Procedimiento Realizado</label>
                                        {!! Form::select('iprocedimiento', $Proce, $Modulo->iprocedimiento,  ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label for="roles">Hemoderivados</label>
                                        {!! Form::select('ihemoderivados', [
                                                                        '1' => 'Si', 
                                                                        '2' => 'No',
                                                                        
                                                                    ], null, ['placeholder' => 'Seleccione...', 'class'=>'form-control']); !!}
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-success"><span class="text-center">Reintervención</span></div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="roles">1</label>
                                        {!! Form::select('ireintervencion1', $Inter, $Modulo->ireintervencion1, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="roles">2</label>
                                        {!! Form::select('ireintervencion2', $Inter, $Modulo->ireintervencion2, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="roles">3</label>
                                        {!! Form::select('ireintervencion3', $Inter, $Modulo->ireintervencion3, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="roles">4</label>
                                        {!! Form::select('ireintervencion4', $Inter, $Modulo->ireintervencion4, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="roles">5</label>
                                        {!! Form::select('ireintervencion5', $Inter, $Modulo->ireintervencion5, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <div class="form-group">
                                    <label for="roles">Hemoderivados</label>
                                    {!! Form::select('ihemoderivados2', [
                                                                    '1' => 'Si', 
                                                                    '2' => 'No',
                                                                    
                                                                ], null, ['placeholder' => 'Seleccione...', 'class'=>'form-control']); !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <div class="form-group">
                                    <label for="roles">Egreso</label>
                                    {!! Form::text('cegreso', null, array('class'=>'form-control')); !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <div class="form-group">
                                    <label for="roles">Mejoria</label>
                                    {!! Form::text('cmejoria', null, array('class'=>'form-control')); !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <div class="form-group">
                                    <label for="roles">Referencia</label>
                                    {!! Form::text('creferencia', null, array('class'=>'form-control')); !!}
                                </div>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <div class="form-group">
                                    <label for="roles">Traslado</label>
                                        {!! Form::select('itraslado', $Trasla, $Modulo->itraslado, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <div class="form-group">
                                    <label for="roles">Traslado Fuera del Estado</label>
                                        {!! Form::select('ifuera_estado', $Fuera, $Modulo->ifuera_estado, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                    <label for="roles">APEO</label>
                                        {!! Form::select('iapeo', $Plani, $Modulo->iapeo, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="roles">Morbilidad Materna Severa</label>
                                        {!! Form::select('imorbilidad', $Morb, $Modulo->imorbilidad, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label for="roles">Mortalidad Materna</label>
                                        {!! Form::select('imortalidad', $Morta, $Modulo->imortalidad, ['placeholder' => 'Seleccione...', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-style'=>'btn-primary']); !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="cobservaciones">Observaciones</label>
                                        {!! Form::text('cobservaciones', null, array('class'=>'form-control')); !!}
                                    </div>
                                </div>
                            </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <button type="submit" class="btn btn-success">Actualizar</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Modulo;
use DataTables;

class ReporteController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reporte.index');
    }

    public function getReporte(Request $request)
    {
        $start = $request['fch1']; 
        $end = $request['fch2'];
        $modulos = Modulo::select(DB::raw("iedad,
        concat(capaterno,' ',camaterno,' ',cnombre)AS nombre,
        itrimestre,
        igestas,
        csdg,
        ipartos,
        icesareas,
        diagnostico_primario,
        hospital,
        traslado,
        fuera,
        metodo,
        morbilidad,
        mortalidad,
        turno,
        procedimiento,
        terminacion,
        iabortos,fch_registro,cobservaciones,ctelefono,(SELECT intervencion FROM intervenciones WHERE ireintervencion1 = intervenciones.id) AS intervencion1,
        (SELECT intervencion FROM intervenciones WHERE ireintervencion2 = intervenciones.id) AS intervencion2,
        (SELECT intervencion FROM intervenciones WHERE ireintervencion3 = intervenciones.id) AS intervencion3,
        (SELECT intervencion FROM intervenciones WHERE ireintervencion4 = intervenciones.id) AS intervencion4,
        (SELECT intervencion FROM intervenciones WHERE ireintervencion5 = intervenciones.id) AS intervencion5,
        (SELECT diagnostico_secundario FROM diag_secundarios WHERE isecundario1 = diag_secundarios.id) AS isecundario1,
        (SELECT diagnostico_secundario FROM diagnostico_tercearios WHERE isecundario2 = diagnostico_tercearios.id) AS isecundario2"))
        ->join('hospitals', 'hospitals.id', '=', 'ihospi_codigo')
        ->join('diag_primarios', 'diag_primarios.id', '=', 'iprincipa')
        ->leftjoin('terminacion_vaginals', 'terminacion_vaginals.id', '=', 'itermina_embarazo')
        ->leftjoin('traslados', 'traslados.id', '=', 'itraslado')
        ->leftjoin('fuera_estados', 'fuera_estados.id', '=', 'ifuera_estado')
        ->leftjoin('planificacions', 'planificacions.id', '=', 'iapeo')
        ->join('morbilidades', 'morbilidades.id', '=', 'imorbilidad')
        ->join('mortalidades', 'mortalidades.id', '=', 'imortalidad')
        ->leftjoin('turnos', 'turnos.id', '=', 'iturno')
        ->leftjoin('procedimientos', 'procedimientos.id', '=', 'iprocedimiento')
        ->whereBetween('fch_registro', [$start, $end])
        ->get();
        return Datatables::of($modulos)
                    
                    ->make(true);
    }
}

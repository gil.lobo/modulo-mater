<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Modulo, Entidad, DiagPrimario, DiagnosticoTerceario, DiagSecundario, FueraEstado, Hospital, Intervencione, Morbilidade, Mortalidade, Planificacion, Procedimiento, TerminacionVaginal, Traslado, Turno, ActualizaRegistro, MuerteMaterna};
Use Alert;

class ModulController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:ver-mater', ['only' => ['index']]);
        $this->middleware('permission:crear-mater', ['only' => ['create', 'store']]);
        $this->middleware('permission:editar-mater', ['only' => ['edit', 'update']]);
        $this->middleware('permission:eliminar-mater', ['only' => ['destroy']]);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $Modulos = Modulo::select('modulos.id','capaterno', 'cnombre', 'camaterno', 'fch_registro', 'modulos.created_at', 'name')
                                ->join('users', 'users.id', '=', 'modulos.user_id')->paginate(10);
        return view('modulomater.index', compact('Modulos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Prima = DiagPrimario::pluck('diagnostico_primario', 'id')->all();
        $Sec = DiagSecundario::pluck('diagnostico_secundario', 'id')->all();
        $Sec2 = DiagnosticoTerceario::pluck('diagnostico_secundario', 'id')->all();
        $Fuera = FueraEstado::pluck('fuera', 'id')->all();
        $Hosp = Hospital::pluck('hospital', 'id')->all();
        $Inter = Intervencione::pluck('intervencion', 'id')->all();
        $Morb = Morbilidade::pluck('morbilidad', 'id')->all();
        $Morta = Mortalidade::pluck('mortalidad', 'id')->all();
        $Plani = Planificacion::pluck('metodo', 'id')->all();
        $Proce = Procedimiento::pluck('procedimiento', 'id')->all();
        $Termi = TerminacionVaginal::pluck('terminacion', 'id')->all();
        $Trasla = Traslado::pluck('traslado', 'id')->all();
        $Turno = Turno::pluck('turno', 'id')->all();
        $Entidad = Entidad::pluck('entidad', 'id')->all();
        $muerteMaterna = MuerteMaterna::pluck('muerte_materna', 'id')->all();
        //dd($Prima);
        //$roles = Role::pluck('name', 'name')->all();
        return view('modulomater.crear', compact('Prima','Sec','Entidad','Sec2','Fuera','Hosp','Inter','Morb','Morta','Plani','Proce','Termi','Trasla','Turno', 'muerteMaterna'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
                                    'capaterno' => 'required',
                                    'cnombre' => 'required', 
                                    'iprincipa' => 'required',
                                    'fch_registro' => 'required',
                                    'iturno' => 'required',
                                    'iedad' => 'required',
                                    'itrimestre' => 'required|integer',
                                    'igestas' => 'required|integer',
                                    'csdg' => 'required',
                                    'ihospi_codigo' => 'required',
                                    'imorbilidad' => 'required',
                                    'imortalidad' => 'required',
                                ]);
        try {
            $Modulo = Modulo::create([
                'fch_registro' => $request->input('fch_registro'),
                'user_id' => auth()->user()->id,
                'iturno' => $request->input('iturno'),
                'iedad' => $request->input('iedad'),
                'capaterno' => $request->input('capaterno'),
                'camaterno' => $request->input('camaterno'),
                'cnombre' => $request->input('cnombre'),
                'itrimestre' => $request->input('itrimestre'),
                'igestas' => $request->input('igestas'),
                'csdg' => $request->input('csdg'),
                'ipartos' => $request->input('ipartos'),
                'icesareas' => $request->input('icesareas'),
                'iabortos' => $request->input('iabortos'),
                'iprincipa' => $request->input('iprincipa'),
                'isecundario1' => $request->input('isecundario1'),
                'isecundario2' => $request->input('isecundario2'),
                'ihospi_codigo' => $request->input('ihospi_codigo'),
                'itermina_embarazo' => $request->input('itermina_embarazo'),
                'iprocedimiento' => $request->input('iprocedimiento'),
                'ireintervencion1' => $request->input('ireintervencion1'),
                'ireintervencion2' => $request->input('ireintervencion2'),
                'ireintervencion3' => $request->input('ireintervencion3'),
                'ireintervencion4' => $request->input('ireintervencion4'),
                'ireintervencion5' => $request->input('ireintervencion5'),
                'itraslado' => $request->input('itraslado'),
                'ifuera_estado' => $request->input('ifuera_estado'),
                'iapeo' => $request->input('iapeo'),
                'imorbilidad' => $request->input('imorbilidad'),
                'imortalidad' => $request->input('imortalidad'),
                'cobservaciones' => $request->input('cobservaciones'),
                'ctelefono' => $request->input('ctelefono'),
                'ientidad' => $request->input('ientidad'),
                'iderechohabiencia' => $request->input('iderechohabiencia'),
                'ireferencia' => $request->input('ireferencia'),
                'iindigena' => $request->input('iindigena'),
                'ihemoderivados' => $request->input('ihemoderivados'),
                'ihemoderivados2' => $request->input('ihemoderivados2'),
                'cegreso' => $request->input('cegreso'),
                'cmejoria' => $request->input('cmejoria'),
                'creferencia' => $request->input('creferencia'),
                'ipuerperio' => $request->input('ipuerperio'),
                'imuerte_materna' => $request->input('imuerte_materna'),
            ]);
            Alert::success('Éxito', 'Información guardada exitosamente');
            return redirect()->route('modulo-mater.index');
        } catch (\Throwable $th) {
            Alert::error('Error', 'Ocurrió un error al procesar la información intente nuevamente');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Prima = DiagPrimario::pluck('diagnostico_primario', 'id')->all();
        $Sec = DiagSecundario::pluck('diagnostico_secundario', 'id')->all();
        $Sec2 = DiagnosticoTerceario::pluck('diagnostico_secundario', 'id')->all();
        $Fuera = FueraEstado::pluck('fuera', 'id')->all();
        $Hosp = Hospital::pluck('hospital', 'id')->all();
        $Inter = Intervencione::pluck('intervencion', 'id')->all();
        $Morb = Morbilidade::pluck('morbilidad', 'id')->all();
        $Morta = Mortalidade::pluck('mortalidad', 'id')->all();
        $Plani = Planificacion::pluck('metodo', 'id')->all();
        $Proce = Procedimiento::pluck('procedimiento', 'id')->all();
        $Termi = TerminacionVaginal::pluck('terminacion', 'id')->all();
        $Trasla = Traslado::pluck('traslado', 'id')->all();
        $Turno = Turno::pluck('turno', 'id')->all();
        $Modulo = Modulo::find($id);
        $Entidad = Entidad::pluck('entidad', 'id')->all();
        $muerteMaterna = MuerteMaterna::pluck('muerte_materna', 'id')->all();
        //$roles = Role::pluck('name', 'name')->all();
        //$userRoles = $user->roles->pluck('name', 'name')->all();
        return view('modulomater.editar', compact('Modulo', 'Prima','Entidad','Sec', 'Sec2','Fuera','Hosp','Inter','Morb','Morta','Plani','Proce','Termi','Trasla','Turno', 'muerteMaterna'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $this->validate($request, [
            'capaterno' => 'required',
            'cnombre' => 'required', 
            'iprincipa' => 'required',
            'fch_registro' => 'required',
            'iturno' => 'required',
            'iedad' => 'required',
            'itrimestre' => 'required',
            'igestas' => 'required',
            'csdg' => 'required',
            'ihospi_codigo' => 'required',
            'imorbilidad' => 'required',
            'imortalidad' => 'required',
        ]);
        //$input = $request->all();
        try {
        $Modulo = Modulo::find($id);
        $Modulo->update([
            'fch_registro' => $request->input('fch_registro'),
            'iturno' => $request->input('iturno'),
            'iedad' => $request->input('iedad'),
            'capaterno' => $request->input('capaterno'),
            'camaterno' => $request->input('camaterno'),
            'cnombre' => $request->input('cnombre'),
            'itrimestre' => $request->input('itrimestre'),
            'igestas' => $request->input('igestas'),
            'csdg' => $request->input('csdg'),
            'ipartos' => $request->input('ipartos'),
            'icesareas' => $request->input('icesareas'),
            'iabortos' => $request->input('iabortos'),
            'iprincipa' => $request->input('iprincipa'),
            'isecundario1' => $request->input('isecundario1'),
            'isecundario2' => $request->input('isecundario2'),
            'ihospi_codigo' => $request->input('ihospi_codigo'),
            'itermina_embarazo' => $request->input('itermina_embarazo'),
            'iprocedimiento' => $request->input('iprocedimiento'),
            'ireintervencion1' => $request->input('ireintervencion1'),
            'ireintervencion2' => $request->input('ireintervencion2'),
            'ireintervencion3' => $request->input('ireintervencion3'),
            'ireintervencion4' => $request->input('ireintervencion4'),
            'ireintervencion5' => $request->input('ireintervencion5'),
            'itraslado' => $request->input('itraslado'),
            'ifuera_estado' => $request->input('ifuera_estado'),
            'iapeo' => $request->input('iapeo'),
            'imorbilidad' => $request->input('imorbilidad'),
            'imortalidad' => $request->input('imortalidad'),
            'cobservaciones' => $request->input('cobservaciones'),
            'ctelefono' => $request->input('ctelefono'),
            'ientidad' => $request->input('ientidad'),
            'iderechohabiencia' => $request->input('iderechohabiencia'),
            'ireferencia' => $request->input('ireferencia'),
            'iindigena' => $request->input('iindigena'),
            'ihemoderivados' => $request->input('ihemoderivados'),
            'ihemoderivados2' => $request->input('ihemoderivados2'),
            'cegreso' => $request->input('cegreso'),
            'cmejoria' => $request->input('cmejoria'),
            'creferencia' => $request->input('creferencia'),
            'ipuerperio' => $request->input('ipuerperio'),
            'imuerte_materna' => $request->input('imuerte_materna'),
        ]);
        ActualizaRegistro::create([
            'user_id' => auth()->user()->id,
            'modulo_id' => $id
        ]);
                    Alert::success('Éxito', 'Información guardada exitosamente');
            return redirect()->route('modulo-mater.index');
        } catch (\Throwable $th) {
            Alert::error('Error', 'Ocurrió un error al procesar la información intente nuevamente');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = Modulo::find($id)->delete();
            Alert::success('Éxito', 'Información guardada exitosamente');
            return redirect()->route('modulo-mater.index');
        } catch (\Throwable $th) {
            Alert::error('Error', 'Ocurrió un error al procesar la información intente nuevamente');
            return redirect()->back();
        }
    }
}

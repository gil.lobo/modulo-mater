EmUk95a6c6LZmUOOUfEMh4ooPSFEQX_-VQsSVerw



<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\{Lista, Lista2, User, GraficaDetalle};
use Illuminate\Support\Facades\Redirect;
use Twilio\Rest\Client; 

class ListaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lista = Lista::select('listas.id', 'listas.user_id', 'nombre', 'apellidos', 'email', 'status', 'telefono')
                                ->join('users', 'users.id', '=', 'listas.user_id')
                                ->where('igrafica', 0)
                                ->orderBy ('users.created_at','ASC')
                                ->get();
        $lista2 = Lista2::select('lista2s.id', 'lista2s.user_id', 'nombre', 'apellidos', 'email')
                                ->join('users', 'users.id', '=', 'lista2s.user_id')
                                ->where('igrafica', 0)
                                ->where('status', false)
                                ->orderBy ('users.created_at','ASC')
                                ->get();
        
        $graficas = GraficaDetalle::select('graficas.id', 'users.nombre', 'graficas.created_at')
                                ->join('users', 'users.id', '=', 'grafica_detalles.user_id')
                                ->join('graficas', 'graficas.id', '=', 'grafica_detalles.grafica_id')
                                ->where('grafica_detalles.iposicion', '1')
                                ->where('graficas.estatus_evento', '=', false)
                                ->get();
                                
        return Inertia::render('Listas/ListaUno/Lista', ['graficas' => $graficas, 'Lista' => $lista, 'Lista2' => $lista2]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lista = Lista::select('listas.id', 'listas.user_id', 'nombre', 'apellidos', 'email', 'telefono', 'status')
                        ->join('users', 'users.id', '=', 'listas.user_id')
                        ->where('igrafica', 0)
                        ->orderBy ('users.created_at','ASC')
                        ->get();
                        //dd($lista);

        return response()->json(
            $lista
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = Lista::find($id, ['user_id']);
        //dd($user->user_id);
        Lista::where('id', $id)
              ->update([
                  'status' => $request->status
                ]);
               User::where('id', $user->user_id)
                ->update([
                    'nombre' => $request->nombre,
                    'apellidos' => $request->apellidos,
                    'telefono' => $request->telefono,
                    'email' => $request->email,
                  ]);  
                  $users = User::find($user->user_id); 
                  //dd($users->nombre);
                  if ($request->status) {
                    $sid    = "AC2d4daff700d014ba738ea8c4d090ad9d"; 
                    $token  = "647223228b4ae2e6f3d45b8268260356"; 
                    $twilio = new Client($sid, $token); 
 
                    $message = $twilio->messages 
                        ->create("whatsapp:+5217341167212", // to 
                                array( 
                                    "from" => "whatsapp:+19362377675",       
                                    "body" => "Hola ".$users->nombre." Se te comunica que el proceso de tus rendimientos ha comenzado con un estatus del 30% saludos https://www.google.com/maps/@18.9618836,-99.2646238,3a,75y,81.33t/data=!3m6!1e1!3m4!1shEOS5w77NDNLe5hIMnetUw!2e0!7i13312!8i6656" ,
                                    //"mediaUrl" => ["https://www.google.com/maps/@18.9618836,-99.2646238,3a,75y,81.33t/data=!3m6!1e1!3m4!1shEOS5w77NDNLe5hIMnetUw!2e0!7i13312!8i6656"],
                                ) 
                        ); 
                  }
                    
 
              return Redirect::route('lista-uno');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

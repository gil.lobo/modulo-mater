Route::get('/Registra', function () {
    return Inertia::render('Socios/socios');
})->name('Registra');

Route::middleware(['auth:sanctum', 'verified'])->post('/registra-socio', [ListaController::class, 'store'])->name('registra-socio');

Route::get('/Bienvenido', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->name('Welcome');
@extends('layouts.app')
@section('page1_css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">


@endsection
@section('content')
<section class="section">
        <div class="section-header">
            <h3 class="page__heading">Reporte de Módulo Máter</h3>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label for="fch1">Fch Inicio</label>
                                            {!! Form::date('fch1', null, array('class'=>'form-control', 'id' => 'fch1')) !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label for="fch2">Fch Final</label>
                                            {!! Form::date('fch2', null, array('class'=>'form-control', 'id' => 'fch2')) !!}
                                        </div>
                                    </div>
                            </div>
                            
                                    
                           <button onclick="reportes()" type="button" class="btn btn-info">Consultar</button>
                           <br><br>
                           <div class="row" id="tbReport" style="display: none;">
                               <div class="col-xs-12 col-sm-12 col-md-12">
                               <table class="table table-striped table table-striped table-bordered dt-responsive nowrap" style="width:100%" id="reporte">
                                <thead>
                                    <th>Nombre</th>
                                    <th>Edad</th>
                                    <th>Fecha de registro</th>
                                    <th>Turno</th>
                                    <th>SDG</th>
                                    <th>Trimestre</th>
                                    <th>Gestas</th>
                                    <th>Partos</th>
                                    <th>Cesáreas</th>
                                    <th>Abortos</th>
                                    <th>Diag. Primario</th>
                                    <th>Diag. Secundario</th>
                                    <th>Diag. Secundario 2</th>
                                    <th>Hospital</th>
                                    <th>Vía de Terminación del Embarazo</th>
                                    <th>Procedimiento Realizado</th>
                                    <th>Reintervención 1</th>
                                    <th>Reintervención 2</th>
                                    <th>Reintervención 3</th>
                                    <th>Reintervención 4</th>
                                    <th>Reintervención 5</th>
                                    <th>Traslado</th>
                                    <th>Traslado Fuera del Estado</th>
                                    <th>APEO</th>
                                    <th>Morbilidad Materna Severa</th>
                                    <th>Mortalidad Materna</th>
                                    <th>Observaciones</th>
                                    <th>Contacto</th>
                                </thead>
                                <tbody>
                                
                                </tbody>
                            </table>
                               </div>
                           
                            <div class="pagination justify-content-end">
                                
                            </div>
                           </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page2_js')
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>





@endsection

@section('scripts')
<script>
    function reportes(){
        if ($("#fch1").val() == '' && $("#fch2").val() == '') {
            iziToast.error({
                title: 'Hey',
                message: 'Debe Colocar una fecha de inicio y una fecha final'
            });
        } else if(Date.parse($("#fch2").val()) < Date.parse($("#fch1").val())) {
            iziToast.error({
                title: 'Hey',
                message: 'La fecha de inicio no puede ser mayor a la fecha final'
            });
        }else{
            $('#reporte').DataTable({
                destroy: true,
            ajax: {
                "url": "{{ route('getReporte') }}",
                "method": 'GET', //usamos el metodo GET
                "data":{
                    'fch1':$("#fch1").val(),
                    'fch2':$("#fch2").val()
                }, 
            },
            responsive:true,
            autoWidth:false,
            dom: 'Bfrtip',
            
            buttons: [ {
                extend: 'excelHtml5',
                autoFilter: true,
                sheetName: 'Exported data',
                title: 'Módulo Mater'
            } ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            
            },
            columns: [
            {data: 'nombre', name: 'cnombre'},
            {data: 'iedad', name: 'iedad'},
            {data: 'fch_registro', name: 'fch_registro'},
            {data: 'turno', name: 'turno'},
            {data: 'csdg', name: 'csdg'},
            {data: 'itrimestre', name: 'itrimestre'},
            
            {data: 'igestas', name: 'igestas'},
            
            
            {data: 'ipartos', name: 'ipartos'},
            {data: 'icesareas', name: 'icesareas'},
            {data: 'iabortos', name: 'iabortos'},
            {data: 'diagnostico_primario', name: 'diagnostico_primario'},
            {data: 'isecundario1', name: 'isecundario1'},
            {data: 'isecundario2', name: 'isecundario2'},
            {data: 'hospital', name: 'hospital'},
            {data: 'terminacion', name: 'terminacion'},
            {data: 'procedimiento', name: 'procedimiento'},
            {data: 'intervencion1', name: 'intervencion1'},
            {data: 'intervencion2', name: 'intervencion2'},
            {data: 'intervencion3', name: 'intervencion3'},
            {data: 'intervencion4', name: 'intervencion4'},
            {data: 'intervencion5', name: 'intervencion5'},
            {data: 'traslado', name: 'traslado'},
            {data: 'fuera', name: 'fuera'},
            {data: 'metodo', name: 'metodo'},
            {data: 'morbilidad', name: 'morbilidad'},
            {data: 'mortalidad', name: 'mortalidad'},
            {data: 'cobservaciones', name: 'cobservaciones'},
            {data: 'ctelefono', name: 'ctelefono'},
        ]
        }); 
        $("#tbReport").show()
        }
        
            
        }
       
</script>
@endsection
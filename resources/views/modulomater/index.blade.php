@extends('layouts.app')
@section('title')
| OMMMM
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading">Observatorio de Morbilidad y Mortalidad Materna de Morelos</h3>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <a class="btn btn-warning" href="{{route('modulo-mater.create')}}">Nuevo</a>
                            <table class="table table-striped mtd">
                                <thead>
                                    <th>Nombre</th>
                                    <th>Fecha de Realización</th>
                                    <th>Usuario</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    @foreach($Modulos as $Modulo)
                                        <tr>
                                            <td>
                                                {{$Modulo->cnombre}} {{$Modulo->capaterno}} {{$Modulo->camaterno}}
                                            </td>
                                            <td>
                                                {{$Modulo->fch_registro}}
                                            </td>
                                            <td>
                                                {{$Modulo->name}}
                                            </td>
                                            <td>
                                                @can('editar-mater')
                                                    <a class="btn btn-info" href="{{route('modulo-mater.edit', $Modulo->id)}}">Editar</a>
                                                @endcan
                                                @can('eliminar-mater')
                                                    {!! Form::open([
                                                            'method' => 'DELETE', 
                                                            'route' => ['modulo-mater.destroy', $Modulo->id],
                                                            'style' => 'display:inline'
                                                        ]) 
                                                    !!}
                                                    {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                                                    {!! Form::close() !!}
                                                    @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination justify-content-end">
                                {!! $Modulos->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


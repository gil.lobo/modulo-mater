<li class="side-menus {{ Request::is('home*') ? 'active' : '' }}">
    <a class="nav-link" href="/home">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
    
</li>
@can('ver-usuario')
<li class="side-menus {{ Request::is('usuarios*') ? 'active' : '' }}">
    <a class="nav-link" href="/usuarios">
        <i class=" fas fa-users"></i><span>Usuarios</span>
    </a>
</li>
@endcan
@can('ver-rol')
<li class="side-menus {{ Request::is('roles*') ? 'active' : '' }}">
    <a class="nav-link" href="/roles">
        <i class=" fas fa-user-lock"></i><span>Roles</span>
    </a>
</li>
@endcan
@can('ver-mater')
<li class="side-menus {{ Request::is('modulo-mater*') ? 'active' : '' }}">
    <a class="nav-link" href="/modulo-mater">
        <i class="fas fa-notes-medical"></i><span>Observatorio</span>
    </a>
</li>
@endcan
@can('ver-mater')
<li class="side-menus {{ Request::is('reporte*') ? 'active' : '' }}">
    <a class="nav-link" href="/reporte">
    <i class="fas fa-chart-bar"></i><span>Reporte</span>
    </a>
</li>
@endcan

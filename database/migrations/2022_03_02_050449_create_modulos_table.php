<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modulos', function (Blueprint $table) {
            $table->id();
            $table->date('fch_registro');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); 
            $table->integer('iturno'); 
            $table->integer('iedad');
            $table->string('capaterno'); 
            $table->string('camaterno')->nullable(); 
            $table->string('cnombre'); 
            $table->integer('itrimestre'); 
            $table->integer('igestas'); 
            $table->string('csdg');
            $table->integer('ipartos')->nullable(); 
            $table->integer('icesareas')->nullable(); 
            $table->integer('iabortos')->nullable(); 
            $table->integer('iprincipa'); 
            $table->integer('isecundario1')->nullable(); 
            $table->integer('isecundario2')->nullable(); 
            $table->integer('ihospi_codigo'); 
            $table->integer('itermina_embarazo')->nullable(); 
            $table->integer('iprocedimiento')->nullable(); 
            $table->integer('ireintervencion1')->nullable(); 
            $table->integer('ireintervencion2')->nullable(); 
            $table->integer('ireintervencion3')->nullable(); 
            $table->integer('ireintervencion4')->nullable(); 
            $table->integer('ireintervencion5')->nullable(); 
            $table->integer('itraslado')->nullable(); 
            $table->integer('ifuera_estado')->nullable(); 
            $table->integer('iapeo')->nullable(); 
            $table->integer('imorbilidad'); 
            $table->integer('imortalidad'); 
            $table->integer('ientidad')->nullable();
            $table->integer('iderechohabiencia')->nullable();
            $table->integer('ireferencia')->nullable();
            $table->integer('iindigena')->nullable();
            $table->integer('ihemoderivados')->nullable();
            $table->integer('ihemoderivados2')->nullable();
            $table->string('cegreso')->nullable();
            $table->string('cmejoria')->nullable();
            $table->string('creferencia')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modulos');
    }
}

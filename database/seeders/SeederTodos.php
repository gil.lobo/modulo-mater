<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class SeederTodos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permisos = [
            'ver-usuario',
            'crear-usuario',
            'editar-usuario',
            'eliminar-usuario',
            'ver-mater',
            'crear-mater',
            'editar-mater',
            'eliminar-mater',
        ];

        foreach ($permisos as $permiso) {
            Permission::create(['name' => $permiso]);
        }
    }
}
